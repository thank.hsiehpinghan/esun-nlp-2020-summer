package idv.hsiehpinghan.esun.nlp.domain.service.impl;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import idv.hsiehpinghan.esun.nlp.domain.model.news.News;
import idv.hsiehpinghan.esun.nlp.domain.service.AmlNameService;
import idv.hsiehpinghan.esun.nlp.infrastructure.rest.Api;
import idv.hsiehpinghan.esun.nlp.utility.NlpUtility;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class AmlNameServiceImpl implements AmlNameService {
	private News news;
	private Api api;
	
	public static AmlNameServiceImpl of(News news, Api api) {
		return new AmlNameServiceImpl(news, api);
	}
	
	@Override
	public Set<String> getNames() {
		List<String> amlSentences = Stream.of(news.getSentences())
				.filter(NlpUtility::isAmlSentence)
				.collect(Collectors.toList());
		return api.getNames(amlSentences)
				.stream()
				.reduce((total, st) -> {
					total.addAll(st);
					return total;})
				.get();
	}

}
