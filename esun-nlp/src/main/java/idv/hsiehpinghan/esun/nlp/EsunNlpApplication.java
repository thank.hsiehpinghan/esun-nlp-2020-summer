package idv.hsiehpinghan.esun.nlp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EsunNlpApplication {

	public static void main(String[] args) {
		SpringApplication.run(EsunNlpApplication.class, args);
	}

}
