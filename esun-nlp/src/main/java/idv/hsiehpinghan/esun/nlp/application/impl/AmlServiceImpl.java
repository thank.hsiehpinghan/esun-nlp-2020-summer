package idv.hsiehpinghan.esun.nlp.application.impl;

import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hankcs.hanlp.HanLP;

import idv.hsiehpinghan.esun.nlp.application.AmlService;
import idv.hsiehpinghan.esun.nlp.domain.model.news.News;
import idv.hsiehpinghan.esun.nlp.domain.service.impl.AmlNameServiceImpl;
import idv.hsiehpinghan.esun.nlp.infrastructure.rest.Api;

@Service
public class AmlServiceImpl implements AmlService {
	@Autowired
	private Api api;

	@Override
	public Set<String> getAmlNames(String news) {
		return AmlNameServiceImpl.of(News.of(HanLP.convertToSimplifiedChinese(news)), api)
				.getNames()
				.stream()
				.map(n -> HanLP.convertToTraditionalChinese(n))
				.collect(Collectors.toSet());
	}

}
