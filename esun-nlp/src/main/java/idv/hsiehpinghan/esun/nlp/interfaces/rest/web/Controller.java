package idv.hsiehpinghan.esun.nlp.interfaces.rest.web;

import java.time.Instant;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import idv.hsiehpinghan.esun.nlp.interfaces.rest.facade.AmlServiceFacade;
import idv.hsiehpinghan.esun.nlp.interfaces.rest.facade.dto.HealthCheckDto;
import idv.hsiehpinghan.esun.nlp.interfaces.rest.facade.dto.InferenceDto;
import idv.hsiehpinghan.esun.nlp.interfaces.rest.web.command.HealthCheckCommand;
import idv.hsiehpinghan.esun.nlp.interfaces.rest.web.command.InferenceCommand;
import reactor.core.publisher.Mono;

@RestController
public class Controller {
	private final String SERVER_UUID = "4509649f-db24-400f-b39c-c338f22bc117";
	private final String CAPTAIN_EMAIL = "thank.hsiehpinghan@gmail.com";
	
	@Autowired
	private AmlServiceFacade facade;
	
	@PostMapping("/healthcheck")
	public Mono<HealthCheckDto> healthcheck(@Valid @RequestBody HealthCheckCommand cmd) {
		return Mono.just(new HealthCheckDto(
				cmd.getEsunUuid(),
				SERVER_UUID,
				CAPTAIN_EMAIL,
				Instant.now().getEpochSecond()));
	}
	
	@PostMapping("/inference")
	public Mono<InferenceDto> inference(@Valid @RequestBody InferenceCommand cmd) {
		return Mono.just(facade.getInference(
				cmd.getEsunUuid(), 
				cmd.getServerUuid(),
				cmd.getNews()));
	}

}
