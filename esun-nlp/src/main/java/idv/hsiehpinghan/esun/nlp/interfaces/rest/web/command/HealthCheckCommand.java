package idv.hsiehpinghan.esun.nlp.interfaces.rest.web.command;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@Getter
public class HealthCheckCommand {
	/**
	 * 玉山 client 傳給開發者的 task id
	 */
	@NotBlank
	@JsonProperty("esun_uuid")
	private String esunUuid;
	/**
	 * 玉山 client 發出 request 的時間 (Unix Epoch Time， 從 1970-01-01T00:00:00 起算的秒數 )
	 */
	@NotNull
	@JsonProperty("esun_timestamp")
	private Long esunTimestamp;
	/**
	 * 若本次 request 失敗或逾時，玉山將重新呼叫的次數，retry 次數為 2 次，每次重新呼叫次數會減 1 。
	 */
	@NotNull
	private Integer retry;
}