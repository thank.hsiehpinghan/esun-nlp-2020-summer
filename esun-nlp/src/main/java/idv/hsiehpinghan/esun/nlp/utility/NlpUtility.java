package idv.hsiehpinghan.esun.nlp.utility;

import java.io.IOException;
import java.util.List;
import java.util.stream.Stream;

import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.corpus.dependency.CoNll.CoNLLSentence;
import com.hankcs.hanlp.corpus.dependency.CoNll.CoNLLWord;
import com.hankcs.hanlp.dependency.IDependencyParser;
import com.hankcs.hanlp.dependency.perceptron.parser.KBeamArcEagerDependencyParser;
import com.hankcs.hanlp.mining.word2vec.WordVectorModel;

public class NlpUtility {
	private static final float SIMILARITY = 0.3f;
	private static final String MODEL_FILE_NAME = "/home/hsiehpinghan/git/esun-nlp-2020-summer/esun-nlp/data/test/word2vec.txt";
	private static final IDependencyParser PARSER;
	private static final WordVectorModel WORD_VECTOR_MODEL;
	static {
		try {
			PARSER = new KBeamArcEagerDependencyParser();
			WORD_VECTOR_MODEL = new WordVectorModel(MODEL_FILE_NAME);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public static boolean isAmlSentence(String sentence) {
		CoNLLSentence tree = PARSER.parse(sentence);
		return Stream.of(tree.getWordArray()).anyMatch(NlpUtility::isAmlWord);
	}
	
	private static boolean isAmlWord(CoNLLWord word) {
		if(word.CPOSTAG.equals("V") == false) {
			return false;
		}	
		if(WORD_VECTOR_MODEL.similarity("罪", HanLP.convertToSimplifiedChinese(word.LEMMA)) < SIMILARITY) {
			return false;
		}
		return true;
	}
}
