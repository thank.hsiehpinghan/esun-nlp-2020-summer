package idv.hsiehpinghan.esun.nlp.domain.service;

import java.util.Set;

public interface AmlNameService {
	Set<String> getNames();
}
