package idv.hsiehpinghan.esun.nlp.interfaces.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.support.WebExchangeBindException;

@ControllerAdvice
public class ExceptionAdvise {
	@ExceptionHandler(WebExchangeBindException.class)
	public ResponseEntity<String> handleWebExchangeBindException(WebExchangeBindException e) {
		return new ResponseEntity<>(toStr(e), HttpStatus.BAD_REQUEST);
	}

	private String toStr(WebExchangeBindException e) {
		return e.getFieldErrors().stream()
			.map(ex -> ex.getField() + " : " + ex.getDefaultMessage())
			.reduce("", (s0, s1) -> s0 + "\n" + s1);
	}
}
