package idv.hsiehpinghan.esun.nlp.infrastructure.rest.dto;

import java.util.List;
import java.util.Set;

import lombok.Data;

@Data
public class NameDto {
	private List<Set<String>> names;
}
