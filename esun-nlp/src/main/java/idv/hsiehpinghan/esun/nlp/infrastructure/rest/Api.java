package idv.hsiehpinghan.esun.nlp.infrastructure.rest;

import java.util.List;
import java.util.Set;

public interface Api {
	List<Set<String>> getNames(List<String> sentences);
}
