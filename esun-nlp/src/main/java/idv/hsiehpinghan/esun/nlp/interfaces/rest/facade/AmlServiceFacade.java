package idv.hsiehpinghan.esun.nlp.interfaces.rest.facade;

import idv.hsiehpinghan.esun.nlp.interfaces.rest.facade.dto.InferenceDto;

public interface AmlServiceFacade {
	InferenceDto getInference(String esunUuid, String serverUuid, String news);
}
