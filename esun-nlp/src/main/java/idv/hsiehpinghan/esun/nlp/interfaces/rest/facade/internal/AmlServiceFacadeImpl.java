package idv.hsiehpinghan.esun.nlp.interfaces.rest.facade.internal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import idv.hsiehpinghan.esun.nlp.application.AmlService;
import idv.hsiehpinghan.esun.nlp.interfaces.rest.facade.AmlServiceFacade;
import idv.hsiehpinghan.esun.nlp.interfaces.rest.facade.dto.InferenceDto;
import idv.hsiehpinghan.esun.nlp.interfaces.rest.facade.internal.assembler.InferenceDtoAssembler;

@Component
public class AmlServiceFacadeImpl implements AmlServiceFacade {
	@Autowired
	private AmlService service;
	@Autowired
	private InferenceDtoAssembler assembler;

	@Override
	public InferenceDto getInference(String esunUuid, String serverUuid, String news) {
		return assembler.toDto(
				esunUuid, 
				serverUuid, 
				service.getAmlNames(news));
	}

}
