package idv.hsiehpinghan.esun.nlp.infrastructure.rest.impl;

import java.net.URI;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import idv.hsiehpinghan.esun.nlp.infrastructure.rest.Api;
import idv.hsiehpinghan.esun.nlp.infrastructure.rest.dto.NameDto;
import idv.hsiehpinghan.esun.nlp.infrastructure.rest.dto.SentenceDto;

@Component
public class ApiImpl implements Api {
	private RestTemplate restTemplate = new RestTemplate();
	@Value("${names_url}")
	private String namesUrl;

	@Override
	public List<Set<String>> getNames(List<String> sentences) {
		SentenceDto dto = SentenceDto.of(sentences);
	    RequestEntity<SentenceDto> request = RequestEntity
	            .post(URI.create(namesUrl))
	            .contentType(MediaType.APPLICATION_JSON)
	            .accept(MediaType.APPLICATION_JSON)
	            .body(dto);
	    ResponseEntity<NameDto> response = restTemplate
	            .exchange(request, new ParameterizedTypeReference<NameDto>(){});
	    return response.getBody().getNames();
	}
	
//	public static void main(String[] args) {
//		SentenceDto sentences = new SentenceDto();
//		sentences.setSentences(new String[] {
//				"瑞穗银行亚洲首席策略师张建泰也认为，美国对香港国安法的回应可能加剧人民币汇率波动性。",
//				"资深评论人士王剑表示，当前人民币汇率保持一个稳定的位置，这是中共系统性操纵的，但是这个代价也是巨大的，有可能掏空中国的经济。他建议大陆朋友的资产以安全为重，不要被当前的假象迷惑，不要去买大陆的任何金融产品，最保险的办法是换点外汇放在家里，千万不要有火中取栗的想法。"
//		});
//		RestTemplate restTemplate = new RestTemplate();
//	    RequestEntity<SentenceDto> request = RequestEntity
//	            .post(URI.create("http://127.0.0.1:5000/api/name"))
//	            .contentType(MediaType.APPLICATION_JSON)
//	            .accept(MediaType.APPLICATION_JSON)
//	            .body(sentences);
//	    ResponseEntity<NameDto> response = restTemplate
//	            .exchange(request, new ParameterizedTypeReference<>(){});
//	    System.err.println(response.getBody().getNames());
//	}
}
