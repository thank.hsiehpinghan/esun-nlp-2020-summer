package idv.hsiehpinghan.esun.nlp.interfaces.rest.facade.dto;

import java.util.Collections;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class InferenceDto {
	/**
	 * 玉山 client 傳給開發者的 task id
	 */
	@NotBlank
	@JsonProperty("esun_uuid")
	private String esunUuid;
	/**
	 * 由開發者自行產生的 uuid。
	 */
	@NotBlank
	@JsonProperty("server_uuid")
	private String serverUuid;
	/**
	 * 找出來的黑名單 string list，如果結果為空，請回傳空的 list，例:[]
	 */
	private List<String> answer = Collections.emptyList();
	/**
	 * inference API 回傳的時間 (Unix Epoch Time，從 1970-01-01T00:00:00 起算的秒數 )
	 */
	@NotNull
	@JsonProperty("server_timestamp")
	private Long serverTimestamp;
}
