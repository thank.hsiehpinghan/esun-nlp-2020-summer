package idv.hsiehpinghan.esun.nlp.interfaces.rest.facade.internal.assembler;

import java.time.Instant;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import idv.hsiehpinghan.esun.nlp.interfaces.rest.facade.dto.InferenceDto;

@Component
public class InferenceDtoAssembler {
	public InferenceDto toDto(String esunUuid, String serverUuid, Set<String> names) {
		return new InferenceDto(
				esunUuid, 
				serverUuid, 
				names.stream().collect(Collectors.toList()),
				Instant.now().getEpochSecond());
	}
}
