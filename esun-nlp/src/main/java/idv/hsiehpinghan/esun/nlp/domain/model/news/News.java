package idv.hsiehpinghan.esun.nlp.domain.model.news;

import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.corpus.dependency.CoNll.CoNLLSentence;
import com.hankcs.hanlp.corpus.dependency.CoNll.CoNLLWord;
import com.hankcs.hanlp.dependency.IDependencyParser;
import com.hankcs.hanlp.mining.word2vec.AbstractVectorModel;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class News {
	private final float SIMILARITY = 0.3f;
	@EqualsAndHashCode.Include
	private int id;
	private String news;

	public static News of(String news) {
		return new News(news.hashCode(), news);
	}

	public String[] getSentences() {
		return news.split("。");
	}
	
}
