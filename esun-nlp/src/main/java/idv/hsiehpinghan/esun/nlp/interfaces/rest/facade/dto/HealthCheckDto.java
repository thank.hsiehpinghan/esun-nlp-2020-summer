package idv.hsiehpinghan.esun.nlp.interfaces.rest.facade.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class HealthCheckDto {
	/**
	 * 玉山 client 傳給開發者的 task id
	 */
	@NotBlank
	@JsonProperty("esun_uuid")
	private String esunUuid;
	/**
	 * 由開發者自行產生的 uuid，接下來 call inference API 時，會利用此 server_uuid 作為識別。
	 */
	@NotBlank
	@JsonProperty("server_uuid")
	private String serverUuid;
	/**
	 * 隊長 email
	 */
	@NotBlank
	@JsonProperty("captain_email")
	private String captainEmail;
	/**
	 * healthcheck API 回傳訊息的時間 (Unix Epoch Time，從 1970-01-01T00:00:00 起算的秒數 )
	 */
	@NotNull
	@JsonProperty("server_timestamp")
	private Long serverTimestamp;
}
