package idv.hsiehpinghan.esun.nlp.application;

import java.util.Set;

public interface AmlService {
	Set<String> getAmlNames(String news);
}
