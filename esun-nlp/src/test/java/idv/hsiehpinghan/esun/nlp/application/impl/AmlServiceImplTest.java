package idv.hsiehpinghan.esun.nlp.application.impl;


import java.io.FileReader;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import com.opencsv.CSVReader;

import idv.hsiehpinghan.esun.nlp.application.AmlService;

@SpringBootTest
public class AmlServiceImplTest {
	@Autowired
	private AmlService service;

	@Test
	public void testGetAmlNames() throws Exception {
		try(FileReader fReader = new FileReader("/home/hsiehpinghan/git/esun-nlp-2020-summer/esun-nlp/tbrain_train_final_0610_v5.csv");
			CSVReader csvReader = new CSVReader(fReader);
		) {
			csvReader.readNext();	// skip first line
			for(String[] strArr : csvReader) {
				String id = strArr[0];
				String news = strArr[2];
				Set<String> expected = toSet(strArr[3]);
				Set<String> actual = service.getAmlNames(news);
				Assertions.assertIterableEquals(
						expected, 
						actual,
						String.format("id(%s): expected(%s) not equals to actual(%s)", id, expected, actual));
			}
		}
	}

	private Set<String> toSet(String str) {
		Set<String> names = new HashSet<>();
		Pattern pattern = Pattern.compile("'(.+?)'");
		Matcher matcher = pattern.matcher(str);
		while(matcher.find()) {
			names.add(matcher.group(1));
		}
		return names;
	}
}
