from flask import Blueprint, jsonify, request, Response
from esun_api.application import nlp_service

CAPTAIN_EMAIL = 'thank.hsiehpinghan@gmail.com'          #
SALT = 'esun-api'

bp = Blueprint('api', __name__, url_prefix='/api')

@bp.route('/name', methods=['POST'])
def name():
    data = request.get_json(force=True) 
    
    print(data['sentences']) 
    names = nlp_service.getNames(data['sentences'])
    return jsonify({'names': names})

