from flask import Flask

from esun_api.interfaces.rest import (
    api
)

app = Flask(__name__)
app.register_blueprint(api.bp)