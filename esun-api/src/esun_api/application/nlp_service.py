import hanlp

recognizer = hanlp.load(hanlp.hanlp.pretrained.ner.MSRA_NER_BERT_BASE_ZH)

def getNames(sentences):
	words = [list(s) for s in sentences]
	tokenses = recognizer(words)
	names = []
	for tokens in tokenses:
		nms = []
		for token in tokens:
			if token[1] != 'NR':
				continue
			if len(token[0]) <= 1:
				continue
			nms.append(token[0])
		names.append(nms)
	return names

